const button = document.querySelector("#kirim");

button.addEventListener("click", () => {
  const dataFormulir = new FormData(document.getElementById("formulir"));

  axios.post("tugas3_225150400111047.php", dataFormulir).then((response) => {
    if (validasiForm()) {
      alert("Mohon isi semua formulir!");
      return;
    }
    document.getElementById("hasil").innerHTML = response.data;
  });
});

function validasiForm() {
  const nama = document.getElementById("nama").value;
  const nim = document.getElementById("nim").value;
  const jenisKelamin = document.querySelector(
    'input[name="jenisKelamin"]:checked'
  );
  const kendaraan = document.getElementById("kendaraan").value;

  if (nama == "" || nim === "" || !jenisKelamin || kendaraan === "") {
    return true;
  } else {
    return false;
  }
}
